package com;

import java.util.Arrays;

public abstract class Human {
    String name;
    String surname;
    int year;
    int iq;
    Family family;
    String[][] scedule = new String[7][2];

    public void welcome() {
        System.out.println("Hello," + family.getPet().nickname);
    }

    public abstract void greetPet();


    public void feed() {
    }

    ;

    @Override
    public String toString() {
        return "com.Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(scedule) +
                '}';
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public Human() {
    }

    public Human(String name, String surname, int year, int iq, String[][] scedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        if (iq <= 100 || iq >= 1) {
            this.iq = iq;
        } else {
            System.out.println("Your number is higher or smaller than 100 and 1,respectively.So,iq level is set 99 by system");
            this.iq = 99;
        }

        this.scedule = scedule;
    }
}

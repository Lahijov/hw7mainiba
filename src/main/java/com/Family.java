package com;


import java.util.ArrayList;
import java.util.Arrays;

public class Family {
    private Human mother;
    Human father;
    Human[] children = new Human[0];
    Pet pet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "com.Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.deepToString(children) +
                ", pet=" + pet +
                '}';
    }

    public Family(Human mother, Human father, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father) {
    }

    public void addChild(Human human) {
        Human[] child = new Human[children.length + 1];
        for (int j = 0; j < children.length; j++) {
            child[j] = children[j];
        }
        child[child.length - 1] = human;
        this.children = child;

    }


    public int sizeBefore;


    public boolean deleteChild(int index) {
        boolean deleted = false;
        Human[] child = new Human[children.length - 1];
        int i = 0;

        try {
            for (int j = 0; j < children.length; j++) {
                if (j != index) {
                    child[i++] = children[j];
                }
            }

            if (index < children.length) {
                deleted = true;
            }
            this.children = child;

            return deleted;
        } catch (
                Exception exception) {

            System.out.println("error");
            return false;
        }
    }


    public int sizeAfter;

    public int countFamily() {

        return 2 + children.length;
    }


    public ArrayList<Human> listOfChilderen() {
        ArrayList<Human> List = new ArrayList<>();

        for (int i = 0; i < children.length; i++) {
            List.add(children[i]);
        }
        return List;
    }
}
